# Exercice angular pour la formation TDD/Craftmanship 

## Requirements

- node
- npm
- @angular/cli `npm install -g @angular/cli`

## Setup

### Exercice

#### Installer les dépendances

```
cd exercice
npm i 
```

#### Démarrer l'application

```
npm start
```

### Correction

#### Installer les dépendances

```
cd correction
npm i 
```

#### Démarrer l'application
```
npm start
```


