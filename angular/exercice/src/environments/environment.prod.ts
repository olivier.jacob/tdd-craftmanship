export const environment = {
  production: true,
  heroesUrl: 'http://localhost:4300/api/heroes',
  heroesSearchByNameUrl: 'http://localhost:4300/api/heroes?name_like='
};
