import { browser, by, element } from 'protractor';

export class HeroListPage {
  navigateTo() {
    return browser.get(`${browser.baseUrl}/heroes/list`) as Promise<any>;
  }

  getTitleText() {
    return element(by.css('h2')).getText() as Promise<string>;
  }
}
