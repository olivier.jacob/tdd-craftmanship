import { browser, logging } from 'protractor';
import { DashboardPage } from './dashboard.po';

describe('Dashboard page', () => {
  let dashboardPage;

  beforeEach(() => {
    dashboardPage = new DashboardPage();
  });

  it('should navigate', () => {
    dashboardPage.navigateTo();
    expect(dashboardPage.getTitleText()).toEqual('Top Heroes');
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
