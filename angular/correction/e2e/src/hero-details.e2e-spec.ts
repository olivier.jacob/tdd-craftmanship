import { browser, logging } from 'protractor';
import { HeroDetailPage } from './hero-details.po';

describe('HeroDetailsPage', () => {
  let heroDetailPage;

  beforeEach(() => {
    heroDetailPage = new HeroDetailPage();
  });

  it('should navigate', () => {
    heroDetailPage.navigateTo();
    expect(heroDetailPage.getTitleText()).toContain('details');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
