import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { MockComponent, MockModule } from 'ng-mocks';
import { HeaderModule } from './core/header/header.module';
import { MessagesComponent } from './core/messages/messages.component';
import { By } from '@angular/platform-browser';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MockModule(HeaderModule),
      ],
      declarations: [
        AppComponent,
        MockComponent(MessagesComponent),
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
  });

  it('should create the app', () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Tower of heroes'`, () => {
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Tower of heroes');
  });

  it('should render title', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Tower of heroes');
  });

  it('should include app-logs component', () => {
    const logComponent = fixture.debugElement.query(By.css('app-logs'));
    expect(logComponent).toBeTruthy();
  });

  it('should include app-header component', () => {
    const logComponent = fixture.debugElement.query(By.css('app-header'));
    expect(logComponent).toBeTruthy();
  });

});
