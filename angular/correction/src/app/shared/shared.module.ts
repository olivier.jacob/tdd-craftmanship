import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccentButtonComponent } from './accent-button/accent-button.component';
import { PrimaryButtonComponent } from './primary-button/primary-button.component';
import { DeleteIconButtonComponent } from './delete-icon-button/delete-icon-button.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { RouterLinkComponent } from './link/router-link.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AccentButtonComponent,
    PrimaryButtonComponent,
    DeleteIconButtonComponent,
    RouterLinkComponent,
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    RouterModule,
  ],
  exports: [
    AccentButtonComponent,
    PrimaryButtonComponent,
    DeleteIconButtonComponent,
    RouterLinkComponent,
  ],
})
export class SharedModule {}
