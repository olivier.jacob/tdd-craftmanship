import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteIconButtonComponent } from './delete-icon-button.component';
import { MockModule } from 'ng-mocks';
import { MatIconModule } from '@angular/material/icon';
import { By } from '@angular/platform-browser';

describe('DeleteIconButtonComponent', () => {
  let component: DeleteIconButtonComponent;
  let fixture: ComponentFixture<DeleteIconButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DeleteIconButtonComponent,
      ],
      imports: [
        MockModule(MatIconModule),
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteIconButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a button', () => {
    const button = fixture.debugElement.query(By.css('button'));
    expect(button).toBeTruthy();
  });

  it('should have a delete mat-icon', () => {
    const icon = fixture.debugElement.query(By.css('mat-icon'));
    expect(icon.nativeElement.textContent).toBe('delete');
  });


});
