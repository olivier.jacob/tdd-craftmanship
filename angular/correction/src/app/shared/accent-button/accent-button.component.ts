import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-accent-button',
  templateUrl: './accent-button.component.html',
  styleUrls: [ './accent-button.component.css' ],
})
export class AccentButtonComponent implements OnInit {

  @Input()
  label: string;

  constructor() { }

  ngOnInit(): void {
  }

}
