import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccentButtonComponent } from './accent-button.component';
import { By } from '@angular/platform-browser';

describe('PrimaryButtonComponent', () => {
  let component: AccentButtonComponent;
  let fixture: ComponentFixture<AccentButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccentButtonComponent ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccentButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a button', () => {
    const button = fixture.debugElement.query(By.css('button'));
    expect(button).toBeTruthy();
  });

  it('label should be uppercased', () => {
    component.label = 'toto';
    fixture.detectChanges();
    const button = fixture.debugElement.query(By.css('button'));
    expect(button.nativeElement.innerText).toBe('TOTO');
  });

});
