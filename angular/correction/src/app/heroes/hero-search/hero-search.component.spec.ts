import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroSearchComponent } from './hero-search.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HeroService } from '../shared/hero.service';
import { MockComponent, MockModule } from 'ng-mocks';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterLinkComponent } from '../../shared/link/router-link.component';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { DebugElement } from '@angular/core';
import { Router } from '@angular/router';
import { HeroDetailComponent } from '../hero-detail/hero-detail.component';

describe('HeroSearchComponent', () => {
  let component: HeroSearchComponent;
  let fixture: ComponentFixture<HeroSearchComponent>;
  let heroService: HeroService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HeroSearchComponent,
        MockComponent(RouterLinkComponent),
      ],
      imports: [
        RouterTestingModule.withRoutes([
          {
            path: 'heroes/detail/:id',
            component: MockComponent(HeroDetailComponent),
          },
        ]),
        MockModule(MatAutocompleteModule),
        MockModule(MatFormFieldModule),
        FormsModule,
        ReactiveFormsModule,
      ],
      providers: [
        {
          provide: HeroService,
          useValue: {
            getHeroes: () => {},
            searchHeroes: term => {},
          },
        },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    heroService = TestBed.inject(HeroService);
    router = TestBed.inject(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display Hero search title', () => {
    const title = fixture.debugElement.query(By.css('h2'));

    // Then
    expect(title.nativeElement.textContent).toBe('Hero search');
  });

  it('should have an input', () => {
    const input = fixture.debugElement.query(By.css('input'));

    // Then
    expect(input.nativeElement).toBeTruthy();
  });

  it('should have an input with a placeholder', () => {
    const input = fixture.debugElement.query(By.css('input'));

    // Then
    expect(input.nativeElement.attributes.placeholder.textContent).toBe('Search by name');
  });

  it('should have an input with a placeholder', () => {
    const input = fixture.debugElement.query(By.css('input'));

    // Then
    expect(input.nativeElement.attributes.placeholder.textContent).toBe('Search by name');
  });

  it('writing text in input should call heroService to search heroes', async () => {
    // Given
    fixture.detectChanges();
    const searchSpy = spyOn(heroService, 'searchHeroes').and.returnValue(of([
      { id: 0, name: 'toto' },
    ]));

    const input = fixture.debugElement.query(By.css('input'));
    input.nativeElement.value = 'to';
    input.nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();


    await fixture.whenStable().then(() => {
      expect(searchSpy).toHaveBeenCalledWith('to');
    });
  });

  it('when there is an array of result in the observable, should display them in a list', async () => {
    // Given
    component.heroes$ = of([
      { id: 0, name: 'toto' },
      { id: 1, name: 'toto' },
      { id: 2, name: 'toto' },
    ]);
    fixture.detectChanges();

    const options: DebugElement[] = fixture.debugElement.queryAll(By.css('mat-option'));
    expect(options.length).toBe(3);
  });

  it('when clicking on hero option with id 0, it should call navigate to hero/details/0', () => {
    const navigateSpy = spyOn(router, 'navigateByUrl');

    // Given
    component.heroes$ = of([
      { id: 0, name: 'toto' },
      { id: 1, name: 'toto' },
      { id: 2, name: 'toto' },
    ]);
    fixture.detectChanges();

    const option: DebugElement = fixture.debugElement.query(By.css('mat-option'));
    option.nativeElement.click();

    fixture.detectChanges();

    expect(navigateSpy).toHaveBeenCalledWith('heroes/detail/0');

  });

  it('trackByHeroId should return hero id', () => {
    // Given
    const hero = { id: 0, name: 'toto' };

    // When
    const id = component.trackByHeroId(1, hero);

    // Then
    expect(id).toBe(0);
  });
});
