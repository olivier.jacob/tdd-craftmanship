import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Hero } from '../shared/hero.model';
import { HeroService } from '../shared/hero.service';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hero-search',
  templateUrl: './hero-search.component.html',
  styleUrls: [ './hero-search.component.css' ],
})
export class HeroSearchComponent implements OnInit {

  heroesControl = new FormControl();
  heroes$: Observable<Hero[]>;
  private URL = 'heroes/detail';

  constructor(
    private router: Router,
    private heroService: HeroService) {}

  ngOnInit(): void {
    this.heroes$ = this.heroesControl.valueChanges
      .pipe(
        // wait 300ms after each keystroke before considering the term
        debounceTime(300),

        // ignore new term if same as previous term
        distinctUntilChanged(),

        // switch to new search observable each time the term changes
        switchMap((term: string) => {
          return this.heroService.searchHeroes(term);
        }),
      );
  }

  navigate(id: number) {
    this.router.navigateByUrl(`${this.URL}/${id}`);
  }

  trackByHeroId(index: number, hero: Hero): number {
    return hero.id;
  }
}
