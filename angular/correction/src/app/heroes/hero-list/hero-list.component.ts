import { Component, OnInit } from '@angular/core';
import { Hero } from '../shared/hero.model';
import { HeroService } from '../shared/hero.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './hero-list.component.html',
  styleUrls: [ './hero-list.component.css' ],
})
export class HeroListComponent implements OnInit {

  heroes: Hero[];
  private URL = 'heroes/detail';

  constructor(
    private router: Router,
    private heroService: HeroService,
  ) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.heroService.addHero({ name } as Hero)
      .subscribe(hero => {
        this.heroes.push(hero);
      });
  }

  delete(hero: Hero): void {
    this.heroService
      .deleteHero(hero)
      .subscribe(() => {
        this.heroes = this.heroes.filter(h => h !== hero);
      });
  }

  navigate(id: number) {
    this.router.navigateByUrl(`${this.URL}/${id}`);
  }
}
