import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { HeroListComponent } from './hero-list/hero-list.component';
import { HeroSearchComponent } from './hero-search/hero-search.component';
import { HeroService } from './shared/hero.service';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { HeroesRoutingModule } from './heroes-routing.module';
import { SharedModule } from '../shared/shared.module';
import { HeroLineComponent } from './hero-line/hero-line.component';
import { TopHeroesResolver } from './shared/top-heroes.resolver';


@NgModule({
  declarations: [
    DashboardComponent,
    HeroDetailComponent,
    HeroListComponent,
    HeroSearchComponent,
    HeroLineComponent,
  ],
  providers: [
    HeroService,
    TopHeroesResolver,
  ],
  imports: [
    CommonModule,
    HeroesRoutingModule,
    SharedModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDividerModule,
    MatListModule,
    MatAutocompleteModule,
    MatCardModule,
    MatInputModule,
  ],
})
export class HeroesModule {}
