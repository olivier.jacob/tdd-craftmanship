import { Component, OnInit } from '@angular/core';
import { Hero } from '../shared/hero.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ],
})
export class DashboardComponent implements OnInit {

  heroes: Hero[];
  private URL = 'heroes/detail';

  constructor(
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.getTopHeroes();
  }

  getTopHeroes(): void {
    this.heroes = this.route.snapshot.data.heroes;
  }

  navigate(id: number) {
    this.router.navigateByUrl(`${this.URL}/${id}`);
  }

  trackByHeroId(index: number, hero: Hero): number {
    return hero.id;
  }
}
