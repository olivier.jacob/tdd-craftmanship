import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponent, MockModule } from 'ng-mocks';
import { HeroSearchComponent } from '../hero-search/hero-search.component';
import { RouterLinkComponent } from '../../shared/link/router-link.component';
import { MatCardModule } from '@angular/material/card';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { HeroDetailComponent } from '../hero-detail/hero-detail.component';
import { Router } from '@angular/router';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let router: Router;
  let routerSpy: jasmine.SpyObj<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DashboardComponent,
        MockComponent(HeroSearchComponent),
        MockComponent(RouterLinkComponent),
      ],
      imports: [
        RouterTestingModule.withRoutes([
          {
            path: 'heroes/details/0',
            component: MockComponent(HeroDetailComponent),
          },
        ]),
        MockModule(MatCardModule),
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.inject(Router);
    routerSpy = spyOn(router, 'navigateByUrl');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have an h2 title display Top Heroes', () => {
    const title = fixture.debugElement.query(By.css('h2'));

    expect(title.nativeElement.textContent).toBe('Top Heroes');
  });

  it('should display list items with hero names', () => {
    // Given
    component.heroes = [ { id: 0, name: 'toto' } ];
    fixture.detectChanges();
    const cards: DebugElement[] = fixture.debugElement.queryAll(By.css('mat-card-title'));
    const firstCard = cards[0];

    // Then
    expect(firstCard.nativeElement.textContent).toBe('toto');
  });

  it('clicking on a hero card should navigate to heroes/detail/{hero.id}', () => {
    // Given
    component.heroes = [ { id: 0, name: 'toto' } ];
    fixture.detectChanges();
    const cards: DebugElement[] = fixture.debugElement.queryAll(By.css('mat-card-title'));
    const firstCard = cards[0];

    // When
    firstCard.nativeElement.click();

    // Then
    expect(routerSpy).toHaveBeenCalledWith('heroes/detail/0');
  });

  it('track function should return the hero id', () => {
    const id = component.trackByHeroId(0, { id: 0, name: 'toto' });

    expect(id).toBe(0);
  });

});
