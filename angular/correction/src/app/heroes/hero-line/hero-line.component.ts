import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Hero } from '../shared/hero.model';

@Component({
  selector: 'app-hero-line',
  templateUrl: './hero-line.component.html',
  styleUrls: [ './hero-line.component.css' ],
})
export class HeroLineComponent implements OnInit {

  @Input()
  hero: Hero;

  @Output()
  select: EventEmitter<Hero> = new EventEmitter<Hero>();

  @Output()
  delete: EventEmitter<Hero> = new EventEmitter<Hero>();

  constructor() { }

  ngOnInit(): void {
  }

  selectHero(hero: Hero) {
    this.select.emit(hero);
  }

  deleteHero(hero: Hero) {
    this.delete.emit(hero);
  }
}
