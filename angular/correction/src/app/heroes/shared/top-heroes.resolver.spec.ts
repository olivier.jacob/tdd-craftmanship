import { async, TestBed } from '@angular/core/testing';

import { TopHeroesResolver } from './top-heroes.resolver';
import { HeroService } from './hero.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

describe('HeroResolverService', () => {
  let sut: TopHeroesResolver;
  let heroService: HeroService;
  let activatedRouteSnapshot: ActivatedRouteSnapshot;
  let routerStateSnapshot: RouterStateSnapshot;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        TopHeroesResolver,
        {
          provide: HeroService,
          useValue: {
            getTopHeroes: () => {},
          },
        },
        {
          provide: ActivatedRouteSnapshot,
          useValue: {},
        },
        {
          provide: RouterStateSnapshot,
          useValue: {},
        },
      ],
    });
  }));

  beforeEach(() => {
    sut = TestBed.inject(TopHeroesResolver);
    heroService = TestBed.inject(HeroService);
    routerStateSnapshot = TestBed.inject(RouterStateSnapshot);
    activatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
  });

  it('resolve should call getTopHeroes', () => {
    const getTopHeroesSpy = spyOn(heroService, 'getTopHeroes');

    // When
    sut.resolve(activatedRouteSnapshot, routerStateSnapshot);

    // Then
    expect(getTopHeroesSpy).toHaveBeenCalled();
  });
});
