import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HeaderComponent } from './header.component';
import { By } from '@angular/platform-browser';
import { MockComponent, MockModule } from 'ng-mocks';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { DebugElement } from '@angular/core';
import { DashboardComponent } from '../../heroes/dashboard/dashboard.component';
import { Location } from '@angular/common';
import { HeroListComponent } from '../../heroes/hero-list/hero-list.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let location: Location;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ HeaderComponent ],
        imports: [
          MockModule(MatToolbarModule),
          MockModule(MatListModule),
          RouterTestingModule.withRoutes([
            {
              path: 'heroes/dashboard',
              component: MockComponent(DashboardComponent),
            },
            {
              path: 'heroes/list',
              component: MockComponent(HeroListComponent),
            },
          ]),
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    location = TestBed.inject(Location);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a the kleis logo', () => {
    const img = fixture.debugElement.query(By.css('img'));
    expect(img.nativeElement.src).toContain('kleis-logo');
  });

  it('a click on the kleis logo should navigate to /heroes/dashboard', async () => {
    // Given
    const link: DebugElement = fixture.debugElement.query(By.css('a#kleisLogo'));

    // When
    link.nativeElement.click();
    fixture.detectChanges();

    // Then
    await fixture.whenStable().then(() => {
      expect(location.path()).toEqual('/heroes/dashboard');
    });
  });

  it('a click on Dashboard link should navigate to /heroes/dashboard', async () => {
    // Given
    const link: DebugElement = fixture.debugElement.query(By.css('a#linkToDashboard'));

    // When
    link.nativeElement.click();
    fixture.detectChanges();

    // Then
    await fixture.whenStable().then(() => {
      expect(location.path()).toEqual('/heroes/dashboard');
    });
  });

  it('a click on the Heroes link should navigate to /heroes/list', async () => {
    // Given
    const link: DebugElement = fixture.debugElement.query(By.css('a#linkToHeroList'));

    // When
    link.nativeElement.click();
    fixture.detectChanges();

    // Then
    await fixture.whenStable().then(() => {
      expect(location.path()).toEqual('/heroes/list');
    });
  });
});
