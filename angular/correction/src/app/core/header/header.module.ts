import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HeaderComponent } from './header.component';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [ HeaderComponent ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatListModule,
    RouterModule,
  ],
  exports: [ HeaderComponent, RouterModule ],
})
export class HeaderModule {}
