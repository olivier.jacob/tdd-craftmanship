import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagesComponent } from './messages.component';
import { MessagesService } from './shared/messages.service';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [ MessagesComponent ],
  providers: [ MessagesService ],
  imports: [
    CommonModule,
    MatListModule,
    MatDividerModule,
    SharedModule,
  ],
  exports: [ MessagesComponent ],
})
export class MessagesModule {}
