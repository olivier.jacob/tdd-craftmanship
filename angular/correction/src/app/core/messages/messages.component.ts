import { Component, OnInit } from '@angular/core';
import { MessagesService } from './shared/messages.service';

@Component({
  selector: 'app-logs',
  templateUrl: './messages.component.html',
  styleUrls: [ './messages.component.css' ],
})
export class MessagesComponent implements OnInit {

  messages: string[];

  constructor(private logsService: MessagesService) { }

  ngOnInit(): void {
    this.messages = this.logsService.getMessages();
  }

  trackByMessage(index: number, message: string): string {
    return message;
  }

  clearMessages(): void {
    this.logsService.clear();
  }
}
