import { MessagesService } from './messages.service';

describe('MessageService', () => {

  it('add should append a message to the list', () => {
    // Given
    const sut: MessagesService = new MessagesService();
    const message = 'salut Toto';

    // When
    sut.add(message);

    // Then
    expect(sut.messages[0]).toBe(message);
  });

  it('clear should empty the message list', () => {
    // Given
    const sut: MessagesService = new MessagesService();
    [ 1, 2, 3, 4, 5 ].forEach(value => sut.add(`message-${value}`));

    // When
    sut.clear();

    // Then
    expect(sut.messages.length).toBe(0);
  });

  it('getMessage should return the message list', () => {
    // Given
    const sut: MessagesService = new MessagesService();
    [ 1, 2, 3, 4, 5 ].forEach(value => sut.add(`message-${value}`));

    // When
    sut.getMessages();

    // Then
    expect(sut.messages.length).toBe(5);
  });

});
