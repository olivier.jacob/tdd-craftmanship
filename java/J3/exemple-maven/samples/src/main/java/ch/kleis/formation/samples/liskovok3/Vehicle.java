package ch.kleis.formation.samples.liskovok3;

public abstract class Vehicle {
    private boolean engineStarted = false;
    int speed = 0;

    public void startEngine() {
        engineStarted = true;
    }

    public void accelerate(int speedIncrease) {
        speed += speedIncrease;
    }
}


