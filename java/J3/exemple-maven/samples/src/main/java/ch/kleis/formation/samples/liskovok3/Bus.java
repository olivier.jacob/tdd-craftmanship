package ch.kleis.formation.samples.liskovok3;

import java.util.ArrayList;
import java.util.List;

public class Bus extends Vehicle implements LuggableVehicle {
    List<Luggage> luggages = new ArrayList<>();

    @Override
    public List<Luggage> getLuggages() {
        return luggages;
    }

}


