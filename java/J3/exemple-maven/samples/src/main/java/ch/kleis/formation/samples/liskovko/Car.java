package ch.kleis.formation.samples.liskovko;

public class Car extends  Vehicle {
    public void startEngine() {
        super.startEngine();
        engageIgnition();
        // ...
    }

    private void engageIgnition() {
        // ...
    }
}
