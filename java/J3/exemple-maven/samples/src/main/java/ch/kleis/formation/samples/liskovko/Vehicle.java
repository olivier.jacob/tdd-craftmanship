package ch.kleis.formation.samples.liskovko;

import java.util.ArrayList;
import java.util.List;

public abstract class Vehicle {
    private boolean engineStarted = false;
    int speed = 0;
    List<Luggage> luggages = new ArrayList<>();

    public void startEngine() {
        engineStarted = true;
    }

    public void accelerate(int speedIncrease) {
        speed += speedIncrease;
    }

    public void storeLuggage(Luggage luggage) {
        luggages.add(luggage);
    }
}
