package ch.kleis.formation.samples.liskovok2;

import java.util.ArrayList;
import java.util.List;

public class Bus extends Vehicle implements LuggableVehicle {
    List<Luggage> luggages = new ArrayList<>();

    @Override
    public void storeLuggable(Luggage newLuggage) {
        luggages.add(newLuggage);
        // Or more complexe....
    }
}


