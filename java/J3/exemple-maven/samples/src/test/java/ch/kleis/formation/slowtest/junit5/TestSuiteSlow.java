package ch.kleis.formation.slowtest.junit5;


import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.platform.suite.api.IncludeTags;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectPackages("ch.kleis.formation.samples.single.ch.kleis.formation")
@IncludeClassNamePatterns({"^.*SlowTest?$"})
@IncludeTags({"slow"})
public class TestSuiteSlow {
}
