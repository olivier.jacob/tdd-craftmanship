package ch.kleis.formation.slowtest.junit5;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;

@Tag("slow")
public class OtherClassSlowTest {
    @Test
    public void testThisOneIsVerySlow() throws InterruptedException {
        // LOAD new DB
        // Load Hibernate
        // Process batches
        Thread.sleep(5000);
        assertFalse(false); // Very stupid too !
    }

}
