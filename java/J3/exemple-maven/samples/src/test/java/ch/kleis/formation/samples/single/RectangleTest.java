package ch.kleis.formation.samples.single;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RectangleTest {

    @ParameterizedTest
    @MethodSource("createRectangles")
    public void computeArea_ShouldReturnArea(Rectangle rectangle) {
        rectangle.setHeight(5);
        rectangle.setWidth(10);
        assertEquals(50, rectangle.computeArea());
    }

    private static Stream<Rectangle> createRectangles() {
//        return Stream.of(new Rectangle(), new Square()); ==> Fail
        return Stream.of(new Rectangle());
    }
}