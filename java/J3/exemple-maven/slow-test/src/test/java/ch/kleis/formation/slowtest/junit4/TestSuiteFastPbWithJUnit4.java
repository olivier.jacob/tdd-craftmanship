package ch.kleis.formation.slowtest.junit4;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Categories.IncludeCategory(FastTests.class)
@Suite.SuiteClasses( { MaClassTest.class, OtherClassSlowTest.class })
public class TestSuiteFastPbWithJUnit4 {
}
