package ch.kleis.formation.slowtest.junit4;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import static junit.framework.TestCase.assertFalse;

public class OtherClassSlowTest {

    @Test
    @Category(SlowTests.class)
    public void testThisOneIsVerySlow() throws InterruptedException {
        // LOAD new DB
        // Load Hibernate
        // Process batches
        Thread.sleep(5000);
        assertFalse(false); // Very stupid too !
    }

}
