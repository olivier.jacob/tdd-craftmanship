package ch.kleis.formation.lags;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class OrderCsvParser {

    public static final String SEPARATOR = ";";

    public String toString(Order order) {
        return order.toString("%1$s;%2$s;%3$d;%4$1.2f");
    }

    public Order parseLine(String line) {
        String[] fields = line.split(SEPARATOR);
        if (fields.length != 4) {
            throw new InvalidFormatException();
        }
        String id = getId(fields);
        LocalDate date = getDate(fields);
        int days = getDaysNumber(fields);
        double price = getPrice(fields);
        return new Order(id, date, days, price);
    }

    private double getPrice(String[] fields) {
        double price;
        try {
            price = Double.parseDouble(fields[3]);
        } catch (NumberFormatException nfe) {
            throw new InvalidPriceException(nfe);
        }
        return price;
    }

    private String getId(String[] fields) {
        return fields[0];
    }

    private int getDaysNumber(String[] fields) {
        int days;
        try {
            days = Integer.parseInt(fields[2]);
        } catch (NumberFormatException nfe) {
            throw new InvalidDaysException(nfe);
        }
        return days;
    }

    private LocalDate getDate(String[] fields) {
        try {
            return LocalDate.parse(fields[1], Order.ORDER_DATE_FORMAT);
        } catch (DateTimeParseException e){
            throw new InvalidDateException(e);
        }
    }

    public class InvalidFormatException extends RuntimeException {
        public InvalidFormatException(Exception e) {
            super(e);
        }

        public InvalidFormatException() {
            super();
        }
    }

    public class InvalidDateException extends InvalidFormatException {
        public InvalidDateException(Exception e) {
            super(e);
        }
    }

    public class InvalidDaysException extends InvalidFormatException {
        public InvalidDaysException(Exception e) {
            super(e);
        }
    }

    public class InvalidPriceException extends InvalidFormatException {
        public InvalidPriceException(Exception e) {
            super(e);
        }
    }
}
