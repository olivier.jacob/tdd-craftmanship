package ch.kleis.formation.lags;

import java.util.List;

public interface OrderDao {

    List<Order> getOrderList();

    void saveOrderList(List<Order> orders);
}
