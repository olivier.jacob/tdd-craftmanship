package ch.kleis.formation.lags;

import dagger.ObjectGraph;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class OrderService {
    @Inject
    OrderCsvParser csvParser;
    @Inject
    OrderDao orderDao;
    @Inject
    SalesComputer salesComputer;
    private List<Order> listOrder = new ArrayList<>();

    public OrderService() {
        ObjectGraph objectGraph = ObjectGraph.create(new OrderModule());
        objectGraph.inject(this);
        // TODO : faire en sorte de ne plus creer un fichier au moment des tests...
        loadOrders();
    }

    private void loadOrders() {
        listOrder = orderDao.getOrderList();
    }

    public void addOrder(String line) {
        Order order = csvParser.parseLine(line);
        listOrder.add(order);
        saveOrders();
    }

    public void deleteOrder(String id) {
        listOrder = listOrder.stream().filter(b -> !b.getId().equals(id)).collect(Collectors.toList());
        saveOrders();
    }

    private void saveOrders() {
        orderDao.saveOrderList(listOrder);
    }

    public Double computeSales() {
        return salesComputer.computeSales(listOrder);
    }

    public List<Order> getOrders() {
        return listOrder;
    }

    void setListOrder(List<Order> listOrder) {
        this.listOrder = listOrder;
    }

    void setOrderDao(OrderFileDao orderDao) {
        this.orderDao = orderDao;
        listOrder = orderDao.getOrderList();
    }
}

