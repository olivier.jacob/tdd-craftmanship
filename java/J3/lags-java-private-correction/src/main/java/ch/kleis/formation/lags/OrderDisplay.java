package ch.kleis.formation.lags;

import java.util.Collections;
import java.util.List;

public class OrderDisplay {

    public String getOrdersDisplayAsText(List<Order> listOrder) {
        Collections.sort(listOrder);
        String orders = "LISTE DES ORDRES\n";
        orders += String.format("%8s %8s %5s %13s", "ID", "DEBUT", "DUREE", "PRIX\n");
        orders += String.format("%8s %8s %5s %13s", "--------", "-------", "-----", "----------\n");
        for (int i = 0; i < listOrder.size(); i++) {
            Order order = listOrder.get(i);
            orders += getOrderDisplayAsText(order);
        }
        orders += String.format("%8s %8s %5s %13s", "--------", "-------", "-----", "----------\n");
        return orders;
    }

    private String getOrderDisplayAsText(Order order) {
        return order.toString("%8s %8s %5d %10.2f\n");
    }
}
