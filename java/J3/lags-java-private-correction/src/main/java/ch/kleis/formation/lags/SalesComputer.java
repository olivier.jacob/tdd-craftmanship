package ch.kleis.formation.lags;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SalesComputer {

    public double computeSales(List<Order> listOrder) {
        Collections.sort(listOrder);
        return computeSalesFromOrderList(listOrder);
    }

    private double computeSalesFromOrderList(List<Order> orders) {
        if (orders.size() == 0) {
            return 0.0;
        }

        Order order = orders.get(0);
        List<Order> consecutiveOrders = getConsecutiveOrders(orders, order);
        List<Order> subListOrders = getSubListOrders(orders);

        double ca = order.getPrice() + computeSalesFromOrderList(consecutiveOrders);
        double ca2 = computeSalesFromOrderList(subListOrders);

        if (Program.DEBUG) {
            System.out.format("%10.2f\n", Math.max(ca, ca2));
        } else {
            System.out.print(".");
        }

        return Math.max(ca, ca2);
    }

    private List<Order> getSubListOrders(List<Order> orders) {
        List<Order> orderSubList = new ArrayList<>();
        if (orders.size() > 0) {
            orderSubList = orders.subList(1, orders.size());
        }
        return orderSubList;
    }

    private List<Order> getConsecutiveOrders(List<Order> orders, Order startOrder) {
        List<Order> liste = new ArrayList<>();
        for (Order o : orders) {
            if (startOrder.appendsBefore(o)) {
                liste.add(o);
            }
        }
        return liste;
    }

}
