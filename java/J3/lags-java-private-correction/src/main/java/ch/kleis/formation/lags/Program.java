package ch.kleis.formation.lags;

import dagger.ObjectGraph;

import javax.inject.Inject;
import java.util.List;
import java.util.Scanner;

class Program {
    public static final boolean DEBUG = false;
    @Inject
    OrderService orderService;
    @Inject
    OrderDisplay orderDisplay;

    public static void main(String[] args) {
        ObjectGraph objectGraph = ObjectGraph.create(new OrderModule());
        Program program = objectGraph.get(Program.class);
        program.run();
    }

    private void run() {
        boolean terminateProgram = false;
        while (!terminateProgram) {
            char command = 'Z';

            while (command != 'A' && command != 'L' && command != 'S' && command != 'Q' && command != 'C') {
                command = getUserCommand();
                terminateProgram = executeCommand(command);
            }
        }
    }

    private boolean executeCommand(char command) {
        boolean quit = false;
        switch (command) {
            case 'Q': {
                quit = true;
                break;
            }
            case 'L': {
                List<Order> listOrder = orderService.getOrders();
                System.out.print(orderDisplay.getOrdersDisplayAsText(listOrder));
                break;
            }
            case 'A': {
                System.out.println("AJOUTER UN ORDRE");
                System.out.println("FORMAT = ID;DEBUT;FIN;PRIX");
                String id = readUserInput();
                orderService.addOrder(id.toUpperCase());
                break;
            }
            case 'S': {
                System.out.println("SUPPRIMER UN ORDRE");
                System.out.println("ID:");
                String id = readUserInput();
                orderService.deleteOrder(id);
                break;
            }
            case 'C': {
                System.out.println("CALCUL CA..");
                Double sales = orderService.computeSales();
                System.out.format("\nCA: %10.2f\n", sales);
                break;
            }
        }
        return quit;
    }

    private String readUserInput() {
        Scanner userInput = new Scanner(System.in);
        String value = "";
        if (userInput.hasNext()) {
            value = userInput.next();
        }
        if (value == null || value.isEmpty()) {
            return readUserInput();
        }
        return value;
    }

    private char getUserCommand() {
        System.out.println("A)JOUTER UN ORDRE  L)ISTER   C)ALCULER CA  S)UPPRIMER  Q)UITTER");
        char commande = 'Z';
        try {
            char keyInfo = (char) System.in.read();
            commande = Character.toUpperCase(keyInfo);
        } catch (java.io.IOException e) {
            System.out.print("IO Exception");
        }
        return commande;
    }
}
