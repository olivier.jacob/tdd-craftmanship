package ch.kleis.formation.lags;

import dagger.Module;
import dagger.Provides;

@Module(injects = {Program.class, OrderService.class})
public class OrderModule {

    @Provides
    OrderCsvParser provideCsvParser() {
        return new OrderCsvParser();
    }

    @Provides
    OrderDao provideOrderDao(OrderCsvParser parser) {
        return new OrderFileDao(parser);
    }

    @Provides
    OrderDisplay providesOrderDisplay() {
        return new OrderDisplay();
    }

    @Provides
    SalesComputer providesSalesComputer() {
        return new SalesComputer();
    }

}
