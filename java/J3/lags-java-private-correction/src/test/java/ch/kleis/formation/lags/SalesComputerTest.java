package ch.kleis.formation.lags;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class SalesComputerTest {

    @Test
    public void computeSales_emptyList_shouldReturn0() {
        // given
        SalesComputer salesComputer = new SalesComputer();
        List<Order> orderList = new ArrayList<>();

        // when
        double ca = salesComputer.computeSales(orderList);

        // then
        Assert.assertEquals(0.00, ca, 0.0);

    }

    @Test
    public void computeSales_oneOrder_shouldComputeOrderPrice() throws ParseException {
        // given
        SalesComputer salesComputer = new SalesComputer();
        List<Order> orderList = new ArrayList<>();
        orderList.add(new Order("Donald", "2015001", 6, 10000.00));

        // when
        double ca = salesComputer.computeSales(orderList);

        // then
        Assert.assertEquals(10000.00, ca, 0.00);
    }

    @Test
    public void computeSales_nonEmptyList_shouldComputeSales() throws ParseException {
        // given
        SalesComputer salesComputer = new SalesComputer();
        List<Order> orderList = new ArrayList<>();
        orderList.add(new Order("Donald", "2015001", 6, 10000.00));
        orderList.add(new Order("Daisy", "2015003", 2, 4000.00));
        orderList.add(new Order("Picsou", "2015007", 7, 8000.00));
        orderList.add(new Order("Mickey", "2015008", 7, 9000.00));

        // when
        double ca = salesComputer.computeSales(orderList);

        // then
        Assert.assertEquals(19000.00, ca, 0.00);
    }

    @Test
    public void computeSales_listShouldBeOrderedForComputeSales() throws ParseException {
        // given
        SalesComputer salesComputer = new SalesComputer();
        List<Order> orderList = new ArrayList<>();
        orderList.add(new Order("Mickey", "2015008", 7, 9000.00));
        orderList.add(new Order("Donald", "2015001", 6, 10000.00));
        orderList.add(new Order("Picsou", "2015007", 7, 8000.00));
        orderList.add(new Order("Daisy", "2015003", 2, 4000.00));

        // when
        double ca = salesComputer.computeSales(orderList);

        // then
        Assert.assertEquals(19000.00, ca, 0.00);
    }

    @Test
    public void computeSales_endOfYear_shouldComputeSales() throws ParseException {
        // given
        SalesComputer salesComputer = new SalesComputer();
        List<Order> orderList = new ArrayList<>();
        orderList.add(new Order("Donald", "2015365", 5, 1000.00));
        orderList.add(new Order("Daisy", "2016001", 5, 2000.00));

        // when
        double ca = salesComputer.computeSales(orderList);

        // then
        Assert.assertEquals(2000.00, ca, 0.00);
    }

    @Test
    public void testDataNonBissextile() {
        // given
        SalesComputer salesComputer = new SalesComputer();
        List<Order> orderList = new ArrayList<>();
        orderList.add(new Order("Donald", "2015365", 2, 10000.00));
        orderList.add(new Order("Daisy", "2016001", 2, 4000.00));

        // when
        double ca = salesComputer.computeSales(orderList);

        // then
        Assert.assertEquals(10000.00, ca, 0.00);
    }



@Test
public void testDataBissextile() throws ParseException {
    // given
    SalesComputer salesComputer = new SalesComputer();
    List<Order> orderList = new ArrayList<>();
    orderList.add(new Order("Donald", "2016365", 2, 10000.00));
    orderList.add(new Order("Daisy", "2017001", 2, 4000.00));

    // when
    double ca = salesComputer.computeSales(orderList);

    // then
    Assert.assertEquals(14000.00, ca, 0.00);
}

}
