package ch.kleis.formation.lags;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class OrderTest {
    @Test
    public void hasNoOverlay_WhenOneOrderIsBeforeTheOther() throws Exception {
        // Given
        Order before = new Order("1", LocalDate.of(2017, 10, 1), 2, 1000);
        Order after = new Order("2", LocalDate.of(2017, 10, 10), 2, 2000);
        // When + Then
        assertFalse(before.hasOverlayWith(after));
    }

    @Test
    public void hasOverlay_WhenFirstOrderStartDuringTheOther() throws Exception {
        // Given
        Order before = new Order("1", LocalDate.of(2017, 10, 1), 10, 1000);
        Order after = new Order("2", LocalDate.of(2017, 10, 5), 10, 2000);
        // When + Then
        assertTrue(before.hasOverlayWith(after));
    }

    @Test
    public void hasOverlay_WhenFirstOrderEndDuringTheOther() throws Exception {
        // Given
        Order before = new Order("1", LocalDate.of(2017, 10, 10), 10, 1000);
        Order after = new Order("2", LocalDate.of(2017, 10, 5), 10, 2000);
        // When + Then
        assertTrue(before.hasOverlayWith(after));
    }

    @Test
    public void hasNoOverlay_WhenOneOrderIsAfterTheOther() throws Exception {
        // Given
        Order before = new Order("1", LocalDate.of(2017, 10, 1), 2, 1000);
        Order after = new Order("2", LocalDate.of(2017, 10, 10), 2, 2000);
        // When + Then
        assertFalse(after.hasOverlayWith(before));
    }

    @Test
    public void appendsBefore_IsTrue_WhenEndFirstOrderIsBeforeTheBeginningOfTheOther() throws Exception {
        // Given
        Order before = new Order("1", LocalDate.of(2017, 10, 1), 2, 1000);
        Order after = new Order("2", LocalDate.of(2017, 10, 3), 2, 2000);
        // When + Then
        assertTrue(before.appendsBefore(after));
    }

    @Test
    public void appendsBefore_IsFalse_WhenEndFirstOrderIsAfterTheBeginningOfTheOther() throws Exception {
        // Given
        Order before = new Order("1", LocalDate.of(2017, 10, 5), 2, 1000);
        Order after = new Order("2", LocalDate.of(2017, 10, 5), 4, 2000);
        // When + Then
        assertFalse(before.appendsBefore(after));
    }

    @Test
    public void appendsBefore_IsFalse_WhenEndFirstOrderIsTheSameAsTheBeginningOfTheOther() throws Exception {
        // Given
        Order before = new Order("1", LocalDate.of(2017, 10, 5), 2, 1000);
        Order after = new Order("2", LocalDate.of(2017, 10, 6), 4, 2000);
        // When + Then
        assertFalse(before.appendsBefore(after));
    }




}