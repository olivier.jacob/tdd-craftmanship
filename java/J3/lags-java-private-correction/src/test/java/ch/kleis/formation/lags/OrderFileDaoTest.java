package ch.kleis.formation.lags;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertTrue;

public class OrderFileDaoTest {

    private static final String fileNotExistsPath = "target/not_existing_file.csv";
    private static final String emptyFilePath = "empty.csv";
    private static final String oneOrderFilePath = "one_order.csv";

    private OrderFileDao orderFileDao;


    @Before
    public void setup() {
        orderFileDao = new OrderFileDao(new OrderCsvParser());
    }

    @Test
    public void getFichierOrder_shouldRetrieveFileOrderAndSetList() throws URISyntaxException {
        // given

        orderFileDao.setFileName(getPath(oneOrderFilePath));

        // when
        List<Order> listOrder = orderFileDao.getOrderList();

        // then
        Assert.assertEquals(1, listOrder.size());

        Order order = listOrder.get(0);
        Assert.assertEquals(new Order("DONALD", "2015001", 6, 10000), order);
    }

    @Test
    public void getFichierOrder_fileNotExists_shouldCreateFile() {
        // given
        String inexistingFile = "/tmp/" + String.valueOf(new Random().nextLong());
        orderFileDao.setFileName(inexistingFile);

        // when
        orderFileDao.getOrderList();

        // then
        assertTrue(Paths.get(inexistingFile).toFile().exists());
        new File(inexistingFile).delete();
    }

    @Test
    public void writeOrdres_noOrder_shouldCreateFileIfNotExists() {
        // given
        String inexistingFile = "/tmp/" + String.valueOf(new Random().nextLong());
        orderFileDao.setFileName(inexistingFile);

        // when
        orderFileDao.saveOrderList(new ArrayList<>());

        // then
        assertTrue(Paths.get(inexistingFile).toFile().exists());
        new File(inexistingFile).delete();
    }

    @Test
    public void writeOrdres_emptyFile_oneOrder_shouldHaveOneLineInFile() throws IOException, ParseException {
        // given
        List<Order> listOrder = Collections.singletonList(new Order("id", "2016042", 42, 666.66));
        orderFileDao.setFileName(getPath(emptyFilePath));

        // when
        orderFileDao.saveOrderList(listOrder);

        // then
        List<String> lines = Files.readAllLines(Paths.get(getPath(emptyFilePath)));
        Assert.assertEquals(1, lines.size());
        Assert.assertEquals("id;2016042;42;666.66", lines.get(0));
    }

    @Test
    public void writeOrdres_nonEmptyFile_oneOrder_shouldAppendOneLineInFile() throws IOException, ParseException {
        // given
        orderFileDao.setFileName(getPath(oneOrderFilePath));
        List<Order> listOrder = orderFileDao.getOrderList();
        listOrder.add(new Order("id", "2016042", 42, 666.66));
        String twoOrdersFilePath = getPath(oneOrderFilePath)+"bis.csv";
        if (new File(twoOrdersFilePath).exists())
            new File(twoOrdersFilePath).delete();

        // when
        orderFileDao.setFileName(twoOrdersFilePath);
        orderFileDao.saveOrderList(listOrder);

        listOrder.add(new Order("id2", "2016043", 89, 10000));
        orderFileDao.saveOrderList(listOrder);

        // then
        List<String> lines = Files.readAllLines(Paths.get(twoOrdersFilePath));
        Assert.assertEquals(3, lines.size());
        Assert.assertEquals("DONALD;2015001;6;10000.00", lines.get(0));
        Assert.assertEquals("id;2016042;42;666.66", lines.get(1));
        Assert.assertEquals("id2;2016043;89;10000.00", lines.get(2));
    }

    @After
    public void tearDown() throws IOException {
        deleteFile(fileNotExistsPath);
    }

    private void deleteFile(String resource) {

        try {
            String file = getPath(resource);
            new File(file).delete();
        } catch (MissingFileException e){
            // Already missing
        }
    }

    @Test
    public void testPathExist() {
        assertTrue(new File(getPath(oneOrderFilePath)).exists());
    }

    @Test(expected = MissingFileException.class)
    public void testPathNotExist() {
        getPath("bad file name");
    }

    public static String getPath(String resource) {
        URL url = OrderFileDaoTest.class.getClassLoader().getResource(resource);
        if (url == null)
            throw new MissingFileException("Missing file resource : " + resource);
        String path = Paths.get(url.getPath()).toAbsolutePath().toString();
        return path;
    }

    private static class MissingFileException extends RuntimeException {
        public MissingFileException(String message) {
            super(message);
        }
    }
}
