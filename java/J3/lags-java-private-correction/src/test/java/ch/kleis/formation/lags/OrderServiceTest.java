package ch.kleis.formation.lags;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.Arrays;

import static org.mockito.BDDMockito.mock;
import static org.mockito.BDDMockito.verify;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.validateMockitoUsage;

public class OrderServiceTest {

    private OrderService orderService;

    @Before
    public void setup() {
        orderService = new OrderService();
    }

    @Test
    public void addOrder_shouldAddOrderInList() {
        // given
        String inputLine = "1;2016042;008;4500.0";
        OrderFileDao orderFileDaoMock = mock(OrderFileDao.class);
        orderService.setOrderDao(orderFileDaoMock);

        // when
        orderService.addOrder(inputLine);

        // then
        Assert.assertEquals(1, orderService.getOrders().size());
        verify(orderFileDaoMock).saveOrderList(anyListOf(Order.class));
    }

    @Test
    public void deleteOrder_shouldDeleteOrderInList() throws ParseException {
        // given
        String inputId = "3";
        OrderFileDao orderFileDaoMock = mock(OrderFileDao.class);
        orderService.setOrderDao(orderFileDaoMock);
        orderService.setListOrder(Arrays.asList(
                new Order("1", "2016032", 4, 9000.0),
                new Order("3", "2016050", 7, 600.0)
        ));

        // when
        orderService.deleteOrder(inputId);

        // then
        int orderListSize = orderService.getOrders().size();
        Assert.assertEquals(1, orderListSize);
        Order order = orderService.getOrders().get(0);
        Assert.assertEquals("1", order.getId());
        verify(orderFileDaoMock).saveOrderList(anyListOf(Order.class));
    }

    @After
    public void validate() {
        validateMockitoUsage();
    }

    @Test(timeout = 4000)
    public void calculerLeCA_shouldBeFastWith160Orders() throws URISyntaxException {
        // given
        OrderFileDao filedao = new OrderFileDao(new OrderCsvParser());
        String name = OrderFileDaoTest.getPath("ORDRES.CSV");
        filedao.setFileName(name);
        orderService.setListOrder(filedao.getOrderList());

        // when + then
        double ca = orderService.computeSales();

        // then
        Assert.assertEquals(327625.3, ca, 0.00);
    }

}



