package ch.kleis.formation.meteo;

class MeteoData {
    private int minTemperature;
    private int maxTemperature;
    private int mediumTemperature;

    MeteoData(int minTemperature, int mediumTemperature, int maxTemperature) {
        this.minTemperature = minTemperature;
        this.maxTemperature = maxTemperature;
        this.mediumTemperature = mediumTemperature;
    }

    public int getMinTemperature() {

        return minTemperature;
    }

    public void setMinTemperature(int minTemperature) {
        this.minTemperature = minTemperature;
    }

    public int getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(int maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public int getMediumTemperature() {
        return mediumTemperature;
    }

    public void setMediumTemperature(int mediumTemperature) {
        this.mediumTemperature = mediumTemperature;
    }
}
