package ch.kleis.formation.meteo;

import org.easymock.EasyMockSupport;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.easymock.EasyMock.*;

public class MeteoStationTest {

    private EasyMockSupport support = new EasyMockSupport();

    private MeteoStation station;
    private MeteoDao meteoDao;
    private Average average;

    @Before
    public void setup() {
        meteoDao = support.mock(MeteoDao.class);
        average = support.mock(Average.class);
        station = new MeteoStation(meteoDao, average);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetMeteoDataForCityRequiredACity(){
        station.getMeteoDataForCity(null);
    }

    @Test
    public void testGetMeteoDataForCityWithOneTemperature(){
        //Given
        expect( meteoDao.getTemperaturesForCity(anyString())).andReturn(Arrays.asList(1, 2, 3));
        // La responsabilité de ce test n'est pas de tester l'implémentation de ch.kleis.formation.meteo.Average !
        expect( average.average(Arrays.asList(1, 2, 3))).andReturn(5);
        support.replayAll();

        //When
        MeteoData measure = station.getMeteoDataForCity("City");

        //Then
        Assert.assertEquals(1, measure.getMinTemperature());
        Assert.assertEquals(5, measure.getMediumTemperature());
        Assert.assertEquals(3, measure.getMaxTemperature());
        support.verifyAll();
    }

}
