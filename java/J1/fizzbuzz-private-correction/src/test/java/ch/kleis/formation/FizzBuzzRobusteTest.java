package ch.kleis.formation;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertEquals;

/**
 * Le code est meilleure : les derniers tests testent uniquement des nouvelles fonctionnalités.
 * Un changement comme "les multiples de 2 doivent renvoyer Ouizz" casse un seul test.
 */

public class FizzBuzzRobusteTest {

    @Test
    public void getFizzbuzz_2_shouldReturnListWith1Element() {

        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        List<String> list = fizzBuzz.getFizzBuzz(1);

        // then

        assertEquals(1, list.size());
    }

    @Test
    public void getFizzBuzz_32_shouldReturnListWith32Elements() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        List<String> list = fizzBuzz.getFizzBuzz(32);

        // then
        assertEquals(32, list.size());
    }

    @Test
    public void getFizzBuzz_First2ElementAreInteger() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        List<String> list = fizzBuzz.getFizzBuzz(2);

        // then
        assertEquals(Arrays.asList("1", "2"), list);

    }

    @Test
    public void getFizzBuzz_MultipleOf5areBuzz() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        List<String> list = fizzBuzz.getFizzBuzz(15);

        // then
        assertEquals("Buzz", list.get(4));
        assertEquals("Buzz", list.get(9));
    }

    @Test
    public void getFizzBuzz_MultipleOf3areFizz() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        List<String> list = fizzBuzz.getFizzBuzz(15);

        // then
        assertEquals("Fizz", list.get(2));
        assertEquals("Fizz", list.get(5));
        assertEquals("Fizz", list.get(8));
    }

    @Test
    public void getFizzBuzz_MultipleOf3And5areFizzBuzz() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        List<String> list = fizzBuzz.getFizzBuzz(50);

        // then
        assertEquals("FizzBuzz", list.get(14));
        assertEquals("FizzBuzz", list.get(44));
    }

    @Test
    public void getFizzBuzz_For0() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        List<String> list = fizzBuzz.getFizzBuzz(0);

        // then
        assertEquals(0, list.size());
    }

    @Test
    public void getFizzBuzz_ForNegativeValueIsSameAsZero() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        List<String> list = fizzBuzz.getFizzBuzz(-1);

        // then
        assertEquals(0, list.size());
    }

    @Test
    public void getFizzBuzz_ValueLarge100AreSameAs100() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        List<String> list = fizzBuzz.getFizzBuzz(110);

        // then
        assertEquals(100, list.size());
    }
}
