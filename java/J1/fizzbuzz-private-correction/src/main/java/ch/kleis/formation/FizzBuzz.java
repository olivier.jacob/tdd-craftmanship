package ch.kleis.formation;

import java.util.ArrayList;
import java.util.List;

public class FizzBuzz {

    public static final int MAX_SIZE = 100;

    public List<String> getFizzBuzz(int size) {
        size = Math.min(size, MAX_SIZE);
        List<String> list = new ArrayList<>();

        for (int i = 1; i <= size; i++) {
            String value = "";
            boolean added = false;
            if (i % 3 == 0) {
                value += "Fizz";
                added = true;
            }
            if (i % 5 == 0) {
                value += "Buzz";
                added = true;
            }
            if (!added) {
                value = String.valueOf(i);
            }
            list.add(value);
        }
        return list;
    }
}
