package ch.kleis.formation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringCalc {

    private List<String> splitToken(String operation) {
        String trimed = operation.trim();
        if (trimed.length() == 0) {
            return new ArrayList<String>();
        }
        String[] asArray = trimed.split("[\\s\\n/#]+");
        List<String> result = new ArrayList<>(Arrays.asList(asArray));
        result.remove("");
        return result;
    }


    public int add(String operation) {
        int total = 0;
        for (String intString : splitToken(operation)) {
            int value = Integer.valueOf(intString);
            if (value < 0) {
                throw new NegativeNumberException();
            }
            if (value < 1000) {
                total += value;
            }
        }
        return total;
    }
}
