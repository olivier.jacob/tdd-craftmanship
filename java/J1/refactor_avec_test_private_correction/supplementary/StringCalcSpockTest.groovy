package ch.kleis.formation

import spock.lang.Specification

class StringCalcSpockTest extends Specification {
    def "Add"() {
        given:
        def stringCalc = new StringCalc()

        when:
        def res = stringCalc.add(input)

        then:
        res == expectedRes

        where:
        input                      | expectedRes
        ""                         | 0
        "42"                       | 42
        "21 21"                    | 42
        "1025 42"                  | 42
//        "  \n "                    | 0
//        "  42 "                    | 42
//        "  42 1000"                | 42
        "12 32"                    | 44
//        "42 \n 23"                 | 65
        new Integer(42).toString() | 42
    }
}
