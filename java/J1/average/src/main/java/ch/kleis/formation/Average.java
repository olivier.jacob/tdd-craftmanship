package ch.kleis.formation;

import java.util.Collection;

public class Average {
    public int average(Collection<Integer> list) {
        if (list.isEmpty()) {
            throw new RuntimeException("Should have one item a least");
        }
        double sumOfValues = 0;
        int nbValues = 0;
        for (Integer curVal : list) {
            sumOfValues += curVal;
            nbValues++;
        }
        return (int) Math.round(sumOfValues / nbValues);
    }
}
