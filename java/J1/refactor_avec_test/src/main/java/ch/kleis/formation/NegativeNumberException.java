package ch.kleis.formation;

public class NegativeNumberException extends RuntimeException {
    public NegativeNumberException() {
        super("Nombre negatif");
    }
}
