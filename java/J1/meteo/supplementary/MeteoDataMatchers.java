package ch.kleis.formation.meteo;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

public class MeteoDataMatchers {

    public static Matcher<MeteoData> isEquivalent(final MeteoData expected) {
        return new BaseMatcher<MeteoData>() {
            @Override
            public boolean matches(final Object item) {
                final MeteoData foo = (MeteoData) item;
                return expected.getMinTemperature() == foo.getMinTemperature() &&
                        expected.getMaxTemperature() == foo.getMaxTemperature() &&
                        expected.getMediumTemperature() == foo.getMediumTemperature();
            }

            @Override
            public void describeMismatch(Object item, Description description) {
                final MeteoData foo = (MeteoData) item;
                if(expected.getMaxTemperature()!= foo.getMaxTemperature()){
                    description.appendText("got max ").appendValue(expected.getMaxTemperature()).appendText(" expected ").appendValue(foo.getMaxTemperature());
                }
                if(expected.getMinTemperature()!= foo.getMinTemperature()){
                    description.appendText("got min ").appendValue(expected.getMinTemperature()).appendText(" expected ").appendValue(foo.getMinTemperature());
                }
                if(expected.getMediumTemperature()!= foo.getMediumTemperature()){
                    description.appendValue("got medium ").appendValue(expected.getMediumTemperature()).appendText(" expected ").appendValue(foo.getMediumTemperature());
                }

            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("expected value ").appendValue(expected);
            }
        };
    }
}
