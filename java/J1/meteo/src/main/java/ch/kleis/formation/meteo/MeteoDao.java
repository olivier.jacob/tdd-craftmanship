package ch.kleis.formation.meteo;

import java.util.List;

interface MeteoDao {

    List<Integer> getTemperaturesForCity(String city);
}
