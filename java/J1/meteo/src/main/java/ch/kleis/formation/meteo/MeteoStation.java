package ch.kleis.formation.meteo;

import java.util.Collections;
import java.util.List;

public class MeteoStation {

    private MeteoDao meteoDao;
    private Average average;

    public MeteoStation(MeteoDao meteoDao, Average average) {
        this.meteoDao = meteoDao;
        this.average = average;
    }

    public MeteoData getMeteoDataForCity(String city) {
        if (city == null) {
            throw new IllegalArgumentException("Should have a city");
        }

        List<Integer> temperatures = meteoDao.getTemperaturesForCity(city);

        int min = Collections.min(temperatures);
        int max = Collections.max(temperatures);
        int avg = average.average(temperatures);

        return new MeteoData(min, avg, max);
    }
}