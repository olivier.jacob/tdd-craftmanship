package ch.kleis.formation.original;

class TextPrinter implements Closeable {
    public void print(String s) {
        println("Bonjour " + s);
    }

    public void close() {
        println("Close");
    }

    void println(String msg) {
        System.out.println(msg);
    }

}
