package ch.kleis.formation.original;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class HtmlPrinterTest {

    @Spy
    private HtmlPrinter underTest;

    @Test
    public void print_ShouldPrintAnHtmlMessage() {
        // Given : no need to explicit the mock method call
        //  doNothing().when(underTest).println("<html> Bonjour message</html>");

        // When
        underTest.print("message");

        // Then
        verify(underTest).println("<html> Bonjour message</html>");
    }

    @Test
    public void close_ShouldPrintACloseMessage() {
        // Given : we can also explicit the mock method call
        doNothing().when(underTest).println(anyString());

        // When
        underTest.close();

        // Then
        verify(underTest).println("Close");
    }
}