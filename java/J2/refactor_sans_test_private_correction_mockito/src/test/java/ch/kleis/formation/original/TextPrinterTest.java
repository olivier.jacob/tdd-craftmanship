package ch.kleis.formation.original;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

public class TextPrinterTest {

    private static class TextPrinterForTest extends TextPrinter {
        private String message;

        @Override
        void println(String msg) {
            this.message = msg;
        }
    }

    private TextPrinterForTest underTest = new TextPrinterForTest();

    @Test
    public void print_ShouldPrintAnHtmlMessage() {
        // Given

        // When
        underTest.print("message");

        // Then
        assertEquals("Bonjour message", underTest.message);
    }

    @Test
    public void close_ShouldPrintACloseMessage() {
        // Given

        // When
        underTest.close();

        // Then
        assertEquals("Close", underTest.message);
    }

}