package ch.kleis.puissance4.exceptions

case class ColumnOutOfBoundsException(column: Int) extends Exception(s"$column")
