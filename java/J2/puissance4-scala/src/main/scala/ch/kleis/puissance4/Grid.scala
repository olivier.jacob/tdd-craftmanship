package ch.kleis.puissance4

import ch.kleis.puissance4.Grid.NUM_COLUMNS
import ch.kleis.puissance4.Pawn.Pawn
import ch.kleis.puissance4.exceptions.{ColumnOutOfBoundsException, ColumnOverflowException}

object Pawn extends Enumeration {
  type Pawn = Value
  val Red = Value(0, "X")
  val Yellow = Value(1, "O")
}

/**
  * la Grid stocke les jetons.
  * La structure est d'avoir un stockage par colonne, chaque colonne étant vide à l'origine
  * chaqu fois qu'on fait ajoute un jeton, on fait grandir une colonne.
  * J'ai donc besoin d'un accès direct sur le pointeur de la colonne, et ensuite, ça peut être séquentiel.
  * @param values
  */
case class Grid(values: Vector[List[Pawn]] = Vector.fill(NUM_COLUMNS) {List.empty}) {
  def countTokens = values.map(_.size).sum
  def isFull = countTokens == Grid.NUM_LINES*Grid.NUM_COLUMNS

  def empty = Grid()

  /**
    * add a pawn, an return a new grid.
    *
    * @param column
    * @param pawn
    * @throws ColumnOverflowException if the column is full
    * @return
    */
  def addPawn(column: Int, pawn: Pawn): Grid = {
    if (column < 0 || column >= Grid.NUM_COLUMNS)
      throw ColumnOutOfBoundsException(column)

    if (values(column).size >= Grid.NUM_LINES)
      throw ColumnOverflowException(column)

    Grid(values.updated(column, values(column) :+ pawn))
  }


  /**
    * transform the grid into a string
    *
    * @return
    */
  override def toString: String = {
    val init = ".........................................."

    //collect position
    val tokenPos = for {
      (colValues, iCol) <- values.zipWithIndex
      (value, iLine) <- colValues.zipWithIndex
    } yield {
      (iCol, iLine, value)
    }

    tokenPos
      //update les positions de la string concaténée
      .foldLeft(init)({ case (str, (iCol, iLine, pawn)) =>
        str.updated((Grid.NUM_LINES - 1 - iLine) * Grid.NUM_COLUMNS + iCol, pawn.toString.charAt(0))
      })
      //on fait du sliding avec une fenêtre Grid.NUM_COLUMNS
      .sliding(Grid.NUM_COLUMNS, Grid.NUM_COLUMNS)
      //join avec \n
      .mkString("\n")
  }
}

object Grid {
  val NUM_COLUMNS = 7
  val NUM_LINES = 6
}
