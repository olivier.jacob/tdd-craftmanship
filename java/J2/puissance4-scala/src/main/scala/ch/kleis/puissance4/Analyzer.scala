package ch.kleis.puissance4

import ch.kleis.puissance4.Pawn.Pawn

class Analyzer {
  val regexpStrings = List(".*____.*", ".*_.{7}_.{7}_.{7}_.*", ".*_.{6}_.{6}_.{6}_.*", ".*_.{8}_.{8}_.{8}_.*")

  val reRed = regexpStrings.map(_.replaceAll("_", "X").r)
  val reYellow = regexpStrings.map(_.replaceAll("_", "O").r)

  def getWinner(grid: Grid): Option[Pawn] = {
    val str = grid.toString.replaceAll("\\s+", "|")

    if(reRed.find(re=> re.findFirstIn(str).isDefined).isDefined){
      Some(Pawn.Red)
    }else if (reYellow.find(re=> re.findFirstIn(str).isDefined).isDefined){
      Some(Pawn.Yellow)
    }else{
      None
    }
  }
}

