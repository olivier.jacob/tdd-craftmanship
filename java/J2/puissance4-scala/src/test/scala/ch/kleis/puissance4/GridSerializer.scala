package ch.kleis.puissance4

object GridSerializer {
  def fromString(str: String): Grid =
    str.trim.replaceAll("\\s", "").reverse.zipWithIndex
      .filter({ case (c, i) => c != '.' })
      .foldLeft(Grid())({ case (grid, (pawnChar, i)) => {
        val pawn = pawnChar match {
          case 'X' => Pawn.Red
          case 'O' => Pawn.Yellow
          case x => throw new IllegalArgumentException(s"unknow Pawn character [$pawnChar]")
        }
        grid.addPawn((Grid.NUM_COLUMNS - 1 - i%Grid.NUM_COLUMNS), pawn)
      }
      })
}
