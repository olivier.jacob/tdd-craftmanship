package ch.kleis.puissance4

import ch.kleis.puissance4.exceptions.{ColumnOutOfBoundsException, ColumnOverflowException}
import org.scalatest.{FlatSpec, Matchers}

class GridTest extends FlatSpec with Matchers {

  behavior of "GridTest"

  "countTokens" should "count 0 with new grid" in {
    val grid = Grid()

    grid.countTokens should equal(0)
  }

  "addToken with one token" should "countToken 1" in {
    val grid = Grid()

    val grid2 = grid.addPawn(0, Pawn.Red)

    grid2.countTokens should equal(1)
  }

  an[ColumnOverflowException] should be thrownBy {
    Grid()
      .addPawn(0, Pawn.Red)
      .addPawn(0, Pawn.Red)
      .addPawn(0, Pawn.Red)
      .addPawn(0, Pawn.Red)
      .addPawn(0, Pawn.Red)
      .addPawn(0, Pawn.Red)
      .addPawn(0, Pawn.Red)
  }

  an[ColumnOutOfBoundsException] should be thrownBy {
    Grid()
      .addPawn(-1, Pawn.Red)
  }

  an[ColumnOutOfBoundsException] should be thrownBy {
    Grid()
      .addPawn(8, Pawn.Red)
  }

  "isFull" should "be true on complete grid" in{
    var grid =GridSerializer.fromString(
      """XXXXXXX
        |XXXXXXX
        |XXXXXXX
        |XXXXXXX
        |XXXXXXX
        |XXXXXXX
        """.stripMargin
    )

    grid.isFull should be(true)
  }

  "empty" should "remove all token" in{
    var grid =GridSerializer.fromString(
      """XXXXXXX
        |XXXXXXX
        |XXXXXXX
        |XXXXXXX
        |XXXXXXX
        |XXXXXXX
      """.stripMargin
    )

    grid.empty.countTokens should be(0)
  }
  "toString" should "print empty" in {
    val grid = Grid()

    val string = grid.toString
    string should equal(
      """.......
        |.......
        |.......
        |.......
        |.......
        |.......""".stripMargin)
  }
  "toString" should "add 2 tokens" in {
    val grid = Grid().addPawn(2, Pawn.Red).addPawn(4, Pawn.Yellow).addPawn(4, Pawn.Red)

    val string = grid.toString

    string should equal(
      """.......
        |.......
        |.......
        |.......
        |....X..
        |..X.O..""".stripMargin)
  }
}
