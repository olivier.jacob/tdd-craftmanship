package ch.kleis.puissance4

import org.scalatest.{FlatSpec, Matchers}

class AnalyzerTest extends FlatSpec with Matchers {
  val analyzer = new Analyzer
  behavior of "Analyzer"

  "read Grid" should "reverse correctly" in {
    val strGrid =
      """.......
        |.......
        |..X....
        |..X....
        |..XO..X
        |.OOO..X""".stripMargin

    val grid = GridSerializer.fromString(strGrid)
    grid.toString should equal(strGrid)
  }

  "no winner" should "answer none" in {
    val grid =GridSerializer.fromString(
      """.......
        |.......
        |..X....
        |..X....
        |..XO..X
        |.OOO..X""".stripMargin
    )
    analyzer.getWinner(grid) should be(None)
  }
  "broken line" should "answer none" in {
    val grid =GridSerializer.fromString(
      """.......
        |.......
        |..X....
        |..X..OO
        |OOXO..X
        |XOOO..X""".stripMargin
    )
    analyzer.getWinner(grid) should be(None)
  }

  "down right diag" should "answer Red" in {
    val grid =GridSerializer.fromString(
      """.......
        |.......
        |..X....
        |..XX.OO
        |..XOXOX
        |..OOXXX""".stripMargin
    )
    analyzer.getWinner(grid) should be(Some(Pawn.Red))
  }
  "up right diag" should "answer Red" in {
    val grid =GridSerializer.fromString(
      """.......
        |.......
        |.....O.
        |...XOO.
        |...OOO.
        |..OOXX.""".stripMargin
    )
    analyzer.getWinner(grid) should be(Some(Pawn.Yellow))
  }

  "column" should "answer Yellow" in {
    val grid =GridSerializer.fromString(
      """.......
        |.......
        |..XO...
        |..XO...
        |..XO..X
        |.OOO..X""".stripMargin
    )
    analyzer.getWinner(grid) should be(Some(Pawn.Yellow))
  }

  "line" should "answer Yellow" in {
    var grid =GridSerializer.fromString(
      """.......
        |.......
        |..XO...
        |..XX...
        |..XO..X
        |.OOOO.X""".stripMargin
    )
    analyzer.getWinner(grid) should be(Some(Pawn.Yellow))
  }


}
