package ch.kleis.puissance4;


import ch.kleis.puissance4.exceptions.PatException;

public class Analyzer {

    /**
     * Teste de le statut de la grille.
     *
     * @param grid
     * @return Pawn.Empty s'il n'y a toujours pas de vainqueur, Pawn.RED ou Pawn.YELLOW si un des 2 joueurs a gagné.
     * @Throws PatException S'il y a à la fois ni vainqueur ni coup à jouer.
     */
    public Pawn analyze(Grid grid) throws PatException {

        for (int column = 1; column <= grid.getColumns(); column++) {
            for (int line = 1; line <= grid.getLines(); line++) {
                for (Direction dir : Direction.values()) {
                    Pawn color = checkFor4PawnsAligned(grid, column, line, dir);
                    if (color != Pawn.EMPTY) {
                        return color;
                    }
                }
            }
        }
        if (grid.isFull()) {
            throw new PatException();
        } else {
            return Pawn.EMPTY;
        }
    }

    private Pawn checkFor4PawnsAligned(Grid grid, int startColunm, int startLine, Direction direction) {
        Pawn color = grid.getPawnForColumnAndLine(startColunm, startLine);
        if (isAlignementOutsideOfGrid(grid, startColunm, startLine, direction)) {
            return Pawn.EMPTY;
        }

        for (int i = 1; i < 4; i++) {
            if (grid.getPawnForColumnAndLine(direction.moveCol(startColunm, i), direction.moveLine(startLine, i)) != color) {
                return Pawn.EMPTY;
            }
        }
        return color;
    }

    private boolean isAlignementOutsideOfGrid(Grid grid, int startColunm, int startLine, Direction direction) {
        int extremColumn = direction.moveCol(startColunm, 3);
        int extremLine = direction.moveLine(startLine, 3);
        if (extremColumn < 1 || extremColumn > grid.getColumns()) {
            return true;
        } else if (extremLine < 1 || extremLine > grid.getLines()) {
            return true;
        } else {
            return false;
        }
    }

}
