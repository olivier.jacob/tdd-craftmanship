package ch.kleis.puissance4;

import java.util.Scanner;

public class Puissance4 {

    private Referee referee;
    private Grid grid;

    Puissance4(Grid grid, Referee referee){
        this.referee = referee;
        this.grid = grid;
    }

    public static void main(String[] args) {
	    Grid grid = new Grid();
	    Analyzer analyzer = new Analyzer();
        Referee referee = new Referee(grid, analyzer);

        new Puissance4(grid, referee).start();
    }

    private void start() {
        while (referee.status() == GameStatus.UNDER_WAY){
            displayGrid();
            referee.insertPawn( readNextStroke());
        }
        System.out.println(referee.status().toString());
    }

    private int readNextStroke() {
        System.out.println("Le joueur courant est : " + referee.nextPlayer() + ", entrez votre colonne [1-7]");
        return new Scanner(System.in).nextInt();
    }

    private void displayGrid() {
        System.out.println(grid.toString());
    }


}
