package ch.kleis.puissance4;

public enum GameStatus {
    UNDER_WAY("La partie continue"), WIN_RED("Le joueur rouge a gagné"), WIN_YELLOW("Le joueur jaune a gagné"), PAT("La partie est nulle");

    private String display;

    private GameStatus(String display) {
        this.display = display;
    }

    @Override
    public String toString() {
        return display;
    }
}
