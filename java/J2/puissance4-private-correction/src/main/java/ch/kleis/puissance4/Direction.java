package ch.kleis.puissance4;

public enum Direction {

    EAST(1, 0),
    NORTH_EAST(1, 1),
    NORTH(0, 1),
    SOUTH_EAST(1, -1);

    private int columnIncrement;
    private int lineIncrement;

    Direction(int columnIncrement, int lineIncrement) {
        this.columnIncrement = columnIncrement;
        this.lineIncrement = lineIncrement;
    }

    public int moveCol(int startPos, int nbMove) {
        return startPos + nbMove * this.columnIncrement;
    }

    public int moveLine(int startPos, int nbMove) {
        return startPos + nbMove * this.lineIncrement;
    }
}
