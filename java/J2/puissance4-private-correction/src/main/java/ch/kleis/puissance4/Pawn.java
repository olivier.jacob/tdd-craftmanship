package ch.kleis.puissance4;


public enum Pawn {
    EMPTY("."), RED("x"), YELLOW("o");

    private String display;

    private Pawn(String display) {
        this.display = display;
    }

    @Override
    public String toString() {
        return display;
    }
}
