package ch.kleis.puissance4;


import ch.kleis.puissance4.exceptions.PatException;

public class Referee {
    private Pawn nextPlayer = Pawn.YELLOW;
    private Grid grid;
    private Analyzer analyzer;

    public Referee(Grid grid, Analyzer analyzer) {
        this.grid = grid;
        this.analyzer = analyzer;
    }

    public Pawn nextPlayer() {
        return nextPlayer;
    }

    public void insertPawn(int column) {
        grid.insertPawn(nextPlayer, column);
        if ( nextPlayer == Pawn.RED){
            nextPlayer = Pawn.YELLOW;
        } else {
            nextPlayer = Pawn.RED;
        }
    }

    public GameStatus status() {
        try {
            Pawn analyze = analyzer.analyze(this.grid);
            if (analyze == Pawn.RED) {
                return GameStatus.WIN_RED;
            } else if (analyze == Pawn.YELLOW) {
                return GameStatus.WIN_YELLOW;
            } else {
                return GameStatus.UNDER_WAY;
            }
        } catch (PatException e) {
            return GameStatus.PAT;
        }
    }
}