package ch.kleis.puissance4;

import ch.kleis.puissance4.exceptions.PatException;
import org.junit.Assert;
import org.junit.Test;

/*
 * Manipule une grille
 * détermine si un joueur gagne :
 *  - 4 jetons de la même couleur alignés (en horizontal, vertical, diagonal)
 *  détermine si la grille est plaine sans victoire
 */
public class AnalyzerTest {
    Analyzer analyzer = new Analyzer();

    @Test
    public void gridWithEmptyGrid_shouldReturnEmpty() {
        // Given
        String strGrid =
                ".......\n" +
                ".......\n" +
                ".......\n" +
                ".......\n" +
                ".......\n" +
                ".......\n";
        // When + Then
        Assert.assertEquals(Pawn.EMPTY, analyzer.analyze(GridTest.parseGrid(strGrid)));
    }


    @Test
    public void gridWithNo4PawnsAligned_shouldReturnEmpty() {
        // Given
        String strGrid =
                ".......\n" +
                ".......\n" +
                "...o...\n" +
                ".x.x...\n" +
                ".ooxo..\n" +
                ".ooxo..\n";
        // When + Then
        Assert.assertEquals(Pawn.EMPTY, analyzer.analyze(GridTest.parseGrid(strGrid)));
    }



    @Test
    public void gridWith4PawnsAlignedHorizontally_shouldReturnColorOfWinner() {
        // Given
        String strGrid =
                ".......\n" +
                ".......\n" +
                ".......\n" +
                ".......\n" +
                ".oooo..\n" +
                ".ooxo..\n";
        // When + Then
        Assert.assertEquals(Pawn.YELLOW, analyzer.analyze(GridTest.parseGrid(strGrid)));
    }

    @Test
    public void gridWith4PawnsAlignedHorizontallyAtBottom_shouldReturnColorOfWinner() {
        // Given
        String strGrid =
                ".......\n" +
                ".......\n" +
                ".......\n" +
                ".......\n" +
                ".......\n" +
                "...oooo\n";
        // When + Then
        Assert.assertEquals(Pawn.YELLOW, analyzer.analyze(GridTest.parseGrid(strGrid)));
    }

    @Test
    public void gridWith4PawnsAlignedVertically_shouldReturnColorOfWinner() {
        // Given
        String strGrid =
                ".......\n" +
                ".o.....\n" +
                ".o.....\n" +
                ".o.....\n" +
                ".o.....\n" +
                ".x.....\n";
        // When + Then
        Assert.assertEquals(Pawn.YELLOW, analyzer.analyze(GridTest.parseGrid(strGrid)));
    }


    @Test
    public void gridWith4PawnsAlignedDiagonalRight_shouldReturnColorOfWinner() {
        // Given
        String strGrid =
                ".......\n" +
                "....x..\n" +
                "...xo..\n" +
                "..xox..\n" +
                ".xooo..\n" +
                ".xxoo..\n";
        // When + Then
        Assert.assertEquals(Pawn.RED, analyzer.analyze(GridTest.parseGrid(strGrid)));
    }

    @Test
    public void gridWith4PawnsAlignedDiagonalLeft_shouldReturnColorOfWinner() {
        // Given
        String strGrid =
                ".......\n" +
                "x......\n" +
                "xx.....\n" +
                "oxx....\n" +
                "xoox...\n" +
                "oxxo...\n";
        // When + Then
        Assert.assertEquals(Pawn.RED, analyzer.analyze(GridTest.parseGrid(strGrid)));
    }

    // Then
    @Test(expected = PatException.class)
    public void gridFilledWithout4PawnsAligned_shouldReturnPatException() {
        // Given
        String strGrid =
                "oooxxox\n" +
                "xxxooxx\n" +
                "xoxooxo\n" +
                "oxxooxx\n" +
                "xooxxox\n" +
                "oxxoxox\n";
        // When
      analyzer.analyze(GridTest.parseGrid(strGrid));
    }
}
