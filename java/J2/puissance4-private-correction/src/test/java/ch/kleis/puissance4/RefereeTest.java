package ch.kleis.puissance4;

import ch.kleis.puissance4.exceptions.PatException;
import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import static org.easymock.EasyMock.*;

@RunWith(EasyMockRunner.class)
public class RefereeTest {

    @Mock
    private Grid grid;

    @Mock
    private Analyzer analyzer;

    @TestSubject
    private Referee referee = new Referee(grid, analyzer);

    @Test
    public void testYellowStartTheGame() {
        assertEquals(Pawn.YELLOW, new Referee(null, null).nextPlayer());
    }

    @Test
    public void testNextStrockIsDelegateForCurrentPlayerYellowAndPlayerChange() {
        // Given
        this.grid.insertPawn(Pawn.YELLOW, 4);
        replay(this.grid);

        // When
        referee.insertPawn(4);

        // Then
        verify(this.grid);
        assertEquals(Pawn.RED, referee.nextPlayer());
    }

    @Test
    public void testGameIsDelegateToAnalyzerWhenGameStillUnderWay() {
        // Given
        expect(this.analyzer.analyze(this.grid)).andReturn(Pawn.EMPTY);
        replay(this.analyzer);

        // When
        GameStatus status = this.referee.status();

        // Then
        assertEquals(GameStatus.UNDER_WAY, status);
        verify(this.analyzer);
    }

    @Test
    public void testGameIsDelegateToAnalyzerWhenGameWinByRed() {
        // Given
        expect(this.analyzer.analyze(this.grid)).andReturn(Pawn.RED);
        replay(this.analyzer);

        // When
        GameStatus status = this.referee.status();

        // Then
        assertEquals(GameStatus.WIN_RED, status);
        verify(this.analyzer);
    }

    @Test
    public void testGameIsDelegateToAnalyzerWhenGameWinByYellow() {
        // Given
        expect(this.analyzer.analyze(this.grid)).andReturn(Pawn.YELLOW);
        replay(this.analyzer);

        // When
        GameStatus status = this.referee.status();

        // Then
        assertEquals(GameStatus.WIN_YELLOW, status);
        verify(this.analyzer);
    }

    @Test
    public void testGameIsDelegateToAnalyzerWhenGameIsPat() {
        // Given
        expect(this.analyzer.analyze(this.grid)).andThrow(new PatException());
        replay(this.analyzer);

        // When
        GameStatus status = this.referee.status();

        // Then
        assertEquals(GameStatus.PAT, status);
        verify(this.analyzer);
    }


}