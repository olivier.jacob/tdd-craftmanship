package ch.kleis.formation.minirefactoring;

import org.easymock.EasyMockSupport;
import org.junit.Test;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class SmartClassOriginalTest extends EasyMockSupport{
    @Test
    public void testHtmlPrinterMissingFirstParam() throws Exception {
        SmartClassOriginal smart = partialMockBuilder(SmartClassOriginal.class)
                .addMockedMethod("exit")
                .createMock();

        smart.exit(anyInt());
        expectLastCall().andThrow(new RuntimeException("Close"));
        replayAll();

        try {
            // When
            smart.smartMain(new String[]{"HtmlPrinter"});
            fail("Should not pass !");
        } catch (RuntimeException e) {
            // Then
            assertEquals("Close", e.getMessage());
        }
        verify(smart);
    }


}