package ch.kleis.formation.tips;

public class Stock
{
      private int quantity;

      Stock( int quantity) {
        this.quantity = quantity;
      }

      public double getPrice() {

          // Appel REST sur une API externe...
          // Simulé par un effet de bord.
        return Math.abs(Math.random());
      }

      public int getQuantity() {
          // Simple accesseur, mais pourrait contenir plus de
          // logique que l'on souhaiterait
        return quantity;
      }

      private void setQuantity(int i){
          this.quantity = i;
      }

      public double getValue() {
        return getPrice() * getQuantity();
      }

}
