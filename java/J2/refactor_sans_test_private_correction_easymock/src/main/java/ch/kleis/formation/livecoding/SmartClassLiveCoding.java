package ch.kleis.formation.livecoding;

@SuppressWarnings("Duplicates")
public class SmartClassLiveCoding {
    public static void main(String[] args) {

      int nameIndex = -1;
      try {
         nameIndex = Integer.parseInt(args[1]);
       }
       catch(Exception ex) { // Trap the number format exception
       }

       String s = new String();

       switch(nameIndex) {
         case 1:
           s = "Henri";
           break;
         case 2:
           s = "Philippe";
           break;
         case 3:
           s = "Cedrik";
           break;
         default:
           System.exit(1);
       }

       Closeable c = null;

       if(args[0] == "HtmlPrinter") {
          HtmlPrinter printer = new HtmlPrinter();
          printer.print(s);
          c = printer;
       }
       else if(args[0] == "TextPrinter") {
          TextPrinter printer = new TextPrinter();
          printer.print(s);
          c = printer;
       }

       c.close();
    }
 }

 interface Closeable {
    void close();
 }

 class HtmlPrinter implements Closeable {
    public void print(String s) {
       System.out.println("<html> Bonjour " + s + "</html>");
    }

    public void close() {
      System.out.println("Close");
    }
 }

 class TextPrinter implements Closeable {
    public void print(String s) {
       System.out.println("Bonjour " + s);
    }

    public void close() {
      System.out.println("Close");
    }
 }
