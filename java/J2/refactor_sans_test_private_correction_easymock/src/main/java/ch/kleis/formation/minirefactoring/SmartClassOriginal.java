/*
 * Copyright (c) 2006 Henri Tremblay
 */
package ch.kleis.formation.minirefactoring;


/**
 * Que pensez-vous de ce code? Que feriez-vous pour l'améliorer au niveau
 * 1- Design
 * 2- Fonctionnement prévu (il y a une erreur qui empêche se programme de fonctionner correctement.
 * 3- Best practices et style de programmation
 * <p>
 * Globalement tout commentaire que tu ferais lors d'une révision de code
 */
public class SmartClassOriginal {
    public static void main(String[] args) {
        new SmartClassOriginal().smartMain(args);
    }

    void smartMain(String[] args) {
        int nameIndex = -1;
        try {
            nameIndex = Integer.parseInt(args[1]);
        } catch (Exception ex) { // Trap the number format exception
        }

        String s = new String();

        switch (nameIndex) {
            case 1:
                s = "Henri";
                break;
            case 2:
                s = "Philippe";
                break;
            case 3:
                s = "Cedrik";
                break;
            default:
                exit(1);
        }

        Closeable c = null;

        if (args[0] == "HtmlPrinter") {
            HtmlPrinter printer = new HtmlPrinter();
            printer.print(s);
            c = printer;
        } else if (args[0] == "TextPrinter") {
            TextPrinter printer = new TextPrinter();
            printer.print(s);
            c = printer;
        }

        c.close();
    }

    public void exit(Integer value) {
        System.exit(value);
    }
}

interface Closeable {
    void close();
}

class HtmlPrinter implements Closeable {
    public void print(String s) {
        System.out.println("<html> Bonjour " + s + "</html>");
    }

    public void close() {
        System.out.println("Close");
    }
}

class TextPrinter implements Closeable {
    public void print(String s) {
        System.out.println("Bonjour " + s);
    }

    public void close() {
        System.out.println("Close");
    }
}
